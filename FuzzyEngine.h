/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include <vector>
#include "Rules/RuleSet.h"
#include "IVariable.h"

/// <summary>
/// The core of the fuzzy logic system, this handles mapping inputs and outputs, as well as managing and updating the 
/// rules.
/// </summary>
class FuzzyEngine {
public:
	FuzzyEngine();
	~FuzzyEngine();

	/// <summary>
	/// Adds a new input or output variable to this fuzzy engine
	/// </summary>
	/// <param name="variable">The variable to add</param>
	void AddVariable(IVariable* variable);

	/// <summary>
	/// Checks whether any of the inputs name's match the given name, and returns the input variable if applicable
	/// </summary>
	/// <param name="name">The name of the input to search for</param>
	/// <param name="out">A pointer to a variable pointer that will store the result, will not be modified if return false</param>
	/// <returns>True if an input variable has a name that matches name, false if otherwise</returns>
	bool HasInput(const std::string& name, IVariable** out = nullptr) const;
	/// <summary>
	/// Gets the number of input variables in this fuzzy engine
	/// </summary>
	size_t NumImputs() const { return myInputs.size(); }
	/// <summary>
	/// Gets the input variable at the given index
	/// </summary>
	InputVariable* GetInput(size_t index) const;

	/// <summary>
	/// Checks whether any of the output name's match the given name, and returns the output variable if applicable
	/// </summary>
	/// <param name="name">The name of the output to search for</param>
	/// <param name="out">A pointer to a variable pointer that will store the result, will not be modified if return false</param>
	/// <returns>True if an output variable has a name that matches name, false if otherwise</returns>
	bool HasOutput(const std::string& name, IVariable** out = nullptr) const;
	/// <summary>
	/// Gets the number of outputs in this fuzzy engine
	/// </summary>
	size_t NumOutputs() const { return myOutputs.size(); }
	/// <summary>
	/// Gets the output variable at the given index
	/// </summary>
	OutputVariable* GetOutput(size_t index) const;
	
	/// <summary>
	/// Adds a new rule set to this fuzzy engine
	/// </summary>
	/// <param name="rules">The rule set to add</param>
	void AddRules(RuleSet* rules);
	/// <summary>
	/// Gets the number of rule sets in this fuzzy engine
	/// </summary>
	size_t NumRules() const { return myRules.size(); }

	/// <summary>
	/// Handles updating all the rule sets, and calculating crisp output values. Not that you
	/// need to update the input variables before calling process
	/// </summary>
	void Process();

private:
	std::vector<InputVariable*> myInputs;
	std::vector<OutputVariable*> myOutputs;
	std::vector<RuleSet*> myRules;
};