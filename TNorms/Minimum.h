/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include "FuzzyPhi/Norms.h"
#include "FuzzyPhi/FuzzyConfig.h"

/// <summary>
/// A simple function that will take the minimum of the 2 values
/// </summary>
class Minimum : public TNorm {
public:
	inline virtual FF_SCALAR Evaluate(FF_SCALAR a, FF_SCALAR b) const { return a < b ? a : b; }
	inline virtual const std::string& GetName() const { return "Minimum"; };
};