/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include "FuzzyPhi/Norms.h"

/// <summary>
/// A simple function that will multiply the two given values together
/// </summary>
class Product : public TNorm {
public:
	inline virtual FF_SCALAR Evaluate(FF_SCALAR a, FF_SCALAR b) const { return a * b; }
	inline virtual const std::string& GetName() const { return "Product"; };
};