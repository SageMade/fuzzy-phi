/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include "FuzzyConfig.h"
#include "Terms/IFuzzyTerm.h"
#include "Norms.h"

#include <vector>

/// <summary>
/// Represents a collection of terms with associated weights
/// This is used by output variables to aggregate triggered sets
/// </summary>
class Aggregate : public IFuzzyTerm {
public:
	/// <summary>
	/// Represents a term that has been activated in this set
	/// </summary>
	struct ActivatedTerm {
		/// <summary>
		/// The term that has been activated
		/// </summary>
		const IFuzzyTerm*  Term;
		/// <summary>
		/// The degree to which it has been activated
		/// </summary>
		FF_SCALAR          Degree;
		/// <summary>
		/// The implication function to use for the term
		/// </summary>
		const TNorm* Implication;

		ActivatedTerm() : Term(nullptr), Degree(0), Implication(nullptr) {}
		ActivatedTerm(const IFuzzyTerm* term, FF_SCALAR degree, const TNorm* implication) : Term(term), Degree(degree), Implication(implication) {}

		FF_SCALAR CalcMembership(FF_SCALAR x) const;
	};

	Aggregate(const std::string& name = "", FF_SCALAR min = -FLT_MAX, FF_SCALAR max = FLT_MAX);
	~Aggregate();

	/// <summary>
	/// Gets the activation degree of a given term in this set
	/// </summary>
	/// <param name="term">The term to search for</param>
	/// <returns>The degree to which term has been activated</returns>
	FF_SCALAR GetActivationDegree(const IFuzzyTerm* term) const;
	/// <summary>
	/// Gets the membership value at x for this aggregated set
	/// </summary>
	/// <param name="x">The value to sample at</param>
	/// <returns>The membership value in this set at x</returns>
	virtual FF_SCALAR GetMembership(FF_SCALAR x) const;
	/// <summary>
	/// This will return the term with the highest level of activation for this aggregate set
	/// </summary>
	/// <returns>The activated term for the set</returns>
	const IFuzzyTerm* GetHighestActivated() const;
	
	/// <summary>
	/// Adds a term to this aggregated set with the given degree and implication functions
	/// </summary>
	/// <param name="term">The term to add</param>
	/// <param name="activationDegree">The degree at which it is activated</param>
	/// <param name="implication">The implication function to use when caluclating term membership with the degree</param>
	void AddTerm(const IFuzzyTerm* term, FF_SCALAR activationDegree, const TNorm* implication);
	/// <summary>
	/// Removes a term from the aggregated set
	/// </summary>
	/// <param name="index">The index of the term to remove</param>
	/// <returns>The term that has been removed</returns>
	const ActivatedTerm& RemoveTerm(int index);
	/// <summary>
	/// Gets the term at the given index in the set
	/// </summary>
	/// <param name="index">The index of the term to get</param>
	/// <returns>The term in the set at index</returns>
	const ActivatedTerm& GetTerm(int index) const;
	/// <summary>
	/// Removes all terms from this aggregated set
	/// </summary>
	void Clear();
	/// <summary>
	/// Gets the number of terms in the aggregated set
	/// </summary>
	size_t NumTerms() const { return myTerms.size(); }

	/// <summary>
	/// Sets the range of this aggregate set
	/// </summary>
	/// <param name="min"></param>
	/// <param name="max"></param>
	void SetRange(FF_SCALAR min, FF_SCALAR max);

	void SetMin(FF_SCALAR value) { myMin = value; }
	FF_SCALAR GetMin() const { return myMin; }

	void SetMax(FF_SCALAR value) { myMax = value; }
	FF_SCALAR GetMax() const { return myMax; }

protected:
	std::vector<ActivatedTerm> myTerms;
	FF_SCALAR myMin, myMax;
	const SNorm* myAggregation;
};