/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#include "FuzzyEngine.h"

FuzzyEngine::FuzzyEngine() {

}

FuzzyEngine::~FuzzyEngine() {

}

void FuzzyEngine::AddVariable(IVariable * variable) {
	if (variable == nullptr)
		throw std::invalid_argument("variable must not be null!");

	// Add the variable to the correct collection depending on it's type
	IVariable::Type type = variable->GetType();
	if (type == IVariable::Input)
		myInputs.push_back((InputVariable*)variable);
	else if (type == IVariable::Output)
		myOutputs.push_back((OutputVariable*)variable);
	else
		throw std::runtime_error("Could not determine variable type");
}

bool FuzzyEngine::HasInput(const std::string& name, IVariable** out) const {
	// Iterate over all inputs
	for (int ix = 0; ix < myInputs.size(); ix++) {
		// If names match
		if (myInputs[ix]->GetName() == name) {
			// If we have specified a value to return to, set the output
			if (out != nullptr)
				*out = myInputs[ix];
			// We found one!
			return true;
		}
	}
	// None were found
	return false;
}

InputVariable* FuzzyEngine::GetInput(size_t index) const {
	if (index >= myInputs.size())
		throw std::out_of_range("index is out of range");
	return myInputs[index];
}

bool FuzzyEngine::HasOutput(const std::string& name, IVariable** out) const {
	// Iterate over all outputs
	for (int ix = 0; ix < myOutputs.size(); ix++) {
		// If names match
		if (myOutputs[ix]->GetName() == name) {
			// If we have specified a value to return to, set the output
			if (out != nullptr)
				*out = myOutputs[ix];
			// We found one!
			return true;
		}
	}
	// None were found
	return false;
}

OutputVariable* FuzzyEngine::GetOutput(size_t index) const {
	if (index >= myOutputs.size())
		throw std::out_of_range("index is out of range");
	return myOutputs[index];
}

void FuzzyEngine::AddRules(RuleSet* rules) {
	myRules.push_back(rules);
}

void FuzzyEngine::Process() {
	// Remove all previously activated terms on outputs
	for (int ix = 0; ix < myOutputs.size(); ix++) { myOutputs[ix]->GetOutputAggregate()->Clear(); }

	// Update all the input values
	for (int ix = 0; ix < myInputs.size(); ix++) { myInputs[ix]->Update(); }

	// Activate all the rules
	for (int ix = 0; ix < myRules.size(); ix++) {
		RuleSet* rules = myRules[ix];
		rules->Activate();
	}

	// Defuzzify all the outputs
	for (int ix = 0; ix < myOutputs.size(); ix++) { myOutputs[ix]->Defuzzify(); }
}
