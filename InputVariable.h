/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once

#include "IVariable.h"

/// <summary>
/// Represents a variable that receives input from the world to make decisions on
/// </summary>
class InputVariable : public IVariable {
public:
	InputVariable(const std::string& name, FF_SCALAR min = FF_SCALAR_MIN, FF_SCALAR max = FF_SCALAR_MAX) : IVariable(name, min, max) { }
	virtual ~InputVariable() {}

	/// <summary>
	/// Gets the pointer that this variable will automatically watch for changes
	/// </summary>
	virtual FF_SCALAR* GetValueRef() const { return myValueRef; }
	/// <summary>
	/// Sets the pointer that this variable will automatically watch for changes
	/// </summary>
	/// <param name="valueRef">A pointer to the value to watch</param>
	virtual InputVariable* SetValueRef(FF_SCALAR* valueRef) { myValueRef = valueRef; return this; }

	virtual Type GetType() const { return Input; };

	/// <summary>
	/// Updates this input, updating the value from the value reference if required
	/// </summary>
	virtual void Update();

private:
	FF_SCALAR* myValueRef;
};

