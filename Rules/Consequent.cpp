/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/

#include "Consequent.h"

#include <stack>
#include <sstream>

#include "FuzzyPhi/IVariable.h"
#include "FuzzyPhi/FuzzyEngine.h"
#include "FuzzyPhi/StringTools.h"

Consequent::~Consequent() {
	for (int ix = 0; ix < myConclusions.size(); ix++)
		delete myConclusions[ix];
	myConclusions.clear();
}

void Consequent::Modify(FF_SCALAR activationDegree, const TNorm* implication) {
	// Iterate over all conclusions
	size_t nc = myConclusions.size();
	for (int ix = 0; ix < nc; ix++) {
		// Get the proposition
		Proposition* prop = myConclusions[ix];
		// As long as the proposition has a variable
		if (prop->Variable != nullptr) {
			// If it is an output
			if (prop->Variable->GetType() == IVariable::Output)
				// Add the proposition's term to the output's aggregate, with the given activation degree and implication function
				((OutputVariable*)prop->Variable)->GetOutputAggregate()->AddTerm(prop->Term, activationDegree, implication);
			else
				throw std::runtime_error("Property variable is not an output!");
		}
	}
}

bool Consequent::IsValid() const {
	return !myConclusions.empty();
}

Consequent* Consequent::Parse(const std::string& expression, const FuzzyEngine* engine) {
	// This is our Working Copy string
	std::string wc = trim_copy(expression);
	if (wc.empty())
		throw std::exception("[Parsing Error] Consequent string is empty");
	enum State {
		State_Variable = 1,
		State_Is = 2,
		State_Term = 4,
		State_And = 8,
		State_With = 16
	};
	Proposition* prop = nullptr;
	std::istringstream tokenizer(wc);
	std::string token;
	State state = State_Variable;
	Consequent* result = new Consequent();
	try {
		// Get the next word in the expression
		while (tokenizer >> token) {
			// If we are expecting a variable
			if (state & State_Variable) {
				IVariable* var = nullptr;
				if (engine->HasOutput(token, &var)) {
					prop = new Proposition();
					prop->Variable = var;
					result->myConclusions.push_back(prop);
					state = State_Is;
					continue;
				}
			}
			// If we are expecting the is statement
			if (state & State_Is) {
				if (token == "is") state = State_Term;
				continue;
			}
			// If we are expecing a term
			if (state & State_Term) {
				if (prop->Variable->HasTerm(token, &prop->Term)) {
					state = (State)(State_And | State_With);
					continue;
				}
			}
			// If we are expecting an operator
			if (state & State_And) {
				if (token == "and") {
					state = State_Variable;
					continue;
				}
			}

			// Should not be possible to get here
			std::ostringstream ex;
			ex << "[Syntax Error] Parse is in unknown state \"" << state << "\" with token \"" << token << "\"";
			throw std::exception(ex.str().c_str());
		}

		// We should not be expecting another variable or operator
		if (!((state & State_With) | (state & State_Variable))) {
			std::ostringstream ex;
			ex << "[Syntax Error] Consequent parse ended in bad state: " << state;
			throw std::exception(ex.str().c_str());
		}
	}
	catch (...) {
		delete result;
		throw;
	}
	return result;
}
