/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once

class RuleSet;

/// <summary>
/// Represents the method for activating rules. For isntance, you may want all rules weighed proportionally against each other, or
/// simply want them to execute in the order they were added
/// </summary>
class Activator {
public:
	virtual ~Activator() {}
	virtual void Activate(RuleSet* rules) = 0;
};