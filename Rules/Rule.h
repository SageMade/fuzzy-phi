/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once

#include "FuzzyPhi/Rules/Antecedent .h"
#include "FuzzyPhi/Norms.h"
#include "FuzzyPhi/Rules/Consequent.h"

class FuzzyEngine;

/// <summary>
/// This rule represents how fuzzy inputs are applied to fuzzy outputs.
/// For instance, a rule may be phrased as "if input a is x, then output a is y"
/// </summary>
class Rule {
public:
	Rule();
	~Rule();

	/// <summary>
	/// Triggers this rule with the given implication function
	/// </summary>
	/// <param name="implication">The implication function to use</param>
	void Trigger(const TNorm* implication);

	/// <summary>
	/// Determines and returns the activation degree using the given functions to calculate
	/// SNorn and TNorm values. The conjuction can be though of as the intersection function, while
	/// the disjunction can be though of as the 
	/// </summary>
	/// <param name="conjuction"></param>
	/// <param name="disjunction"></param>
	FF_SCALAR ActivateWith(const TNorm* conjuction, const SNorm* disjunction);

    /// <summary>
    /// Gets the activation degree of this rule, only valid after calling ActivateWith
    /// </summary>
	FF_SCALAR GetActivationDegree() const { return myActivationDegree; }

	/// <summary>
	/// Resets this rule to it's untriggered state
	/// </summary>
	void Reset();

	/// <summary>
	/// Gets whether this rule has been triggered or not
	/// </summary>
	bool IsTriggered() const { return isTriggered; }
	
	/// <summary>
	/// Gets whether or not this rule is valid in it's current state
	/// </summary>
	bool IsValid() const;

	/// <summary>
	/// Gets the weight of this rule, by default this is 1
	/// </summary>
	FF_SCALAR GetWeight() const { return myWeight; }
	/// <summary>
	/// Sets the weight of this rule
	/// </summary>
	/// <param name="value">The weight of this rule, must be greater than zero</param>
	void SetWeight(FF_SCALAR value);

	/// <summary>
	/// Sets the antecedent clause for this rule. For instance, in the statement
	/// "if a is x then y is b"
	/// the antecedent clause would be "a is x"
	/// </summary>
	/// <param name="andecedent">The antecedent clause for this rule</param>
	void SetAntecedent(Antecedent* antecedent);
	/// <summary>
	/// Gets the antecedent clause for this rule, see SetConsequent for a description of this value
	/// </summary>
	Antecedent* GetAntecedent() const { return myAntecedent; }
	
	/// <summary>
	/// Sets the consequent clause for this rule. For isntance, in the statement
	/// "if a is x then y is b"
	/// the consequent clause would be "y is b"
	/// </summary>
	/// <param name="consequent"></param>
	void SetConsequent(Consequent* consequent);
	/// <summary>
	/// Gets the consequent clause for this rule, see SetConsequent for a description of this value
	/// </summary>
	Consequent* GetConsequent() const { return myConsequent; }

	/// <summary>
	/// Handles parsing a new rule from a given expression and engine context
	/// </summary>
	/// <param name="expression">The expression to parse</param>
	/// <param name="engine">The engine context to parse with</param>
	/// <returns>The rule as loaded from the expression</returns>
	static Rule* Parse(const std::string& expression, const FuzzyEngine* engine);

private:
	Antecedent* myAntecedent;
	Consequent* myConsequent;

	FF_SCALAR myWeight;
	bool      isTriggered;
	FF_SCALAR myActivationDegree;
};

