/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include "FuzzyPhi/Rules/Activator.h"
#include "FuzzyPhi/Rules/RuleSet.h"

/// <summary>
/// Represents a general purpose activator, this will simply run all rules in the order that they were entered
/// </summary>
class GeneralActivator : public Activator {
public:
	virtual void Activate(RuleSet* rules);

protected:
};