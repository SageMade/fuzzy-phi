/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/

#include "GeneralActivator.h"

void GeneralActivator::Activate(RuleSet * rules) {
	// Grab the functions from the rule set
	const TNorm* conjuction = rules->GetConjunction();
	const SNorm* disjunction = rules->GetDisjunction();
	const TNorm* implication = rules->GetImplication();

	// Iterate over all ruels
	size_t nr = rules->NumRules();
	for (int ix = 0; ix < nr; ix++) {
		Rule* rule = rules->GetRule(ix);
		// Reset the rule to it's default state (clears activation degree)
		rule->Reset();
		// Activate the rule, using the provided fuzzy and and or functions
		rule->ActivateWith(conjuction, disjunction);
		// Trigger the rule with the provided implication function
		rule->Trigger(implication);
	}
}
