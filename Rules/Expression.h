/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include "FuzzyPhi/Terms/IFuzzyTerm.h"
#include "FuzzyPhi/IVariable.h"

/// <summary>
/// Represents an expression that can be evaluated as part of an Antecedent or Consequent
/// </summary>
class Expression {
public:
	enum Type {
		Proposition,
		Operator
	};
	virtual ~Expression() {}

	virtual Type GetType() const = 0;
};

class Proposition : public Expression {
public:
	virtual Type GetType() const { return Type::Proposition; }

	IFuzzyTerm* Term;
	IVariable*  Variable;
};

class Operator : public Expression {
public:
	enum ModeType {
		AND,
		OR,
		NOT
	};
	virtual Type GetType() const { return Type::Operator; }

	Expression* Left;
	Expression* Right;
	ModeType Mode;
};