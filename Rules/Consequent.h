/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once

#include <vector>
#include "FuzzyPhi/Norms.h"
#include "FuzzyPhi/Rules/Expression.h"
#include "FuzzyPhi/FuzzyConfig.h"

class FuzzyEngine;

/// <summary>
/// Represents the result of a condition. For instance, in the statement
/// "if a is x then b is y"
/// the "b is y" portion is the consequent
/// </summary>
class Consequent {
public:
	Consequent() {}
	virtual ~Consequent();

	/// <summary>
	/// Modifies the results of this consequent by the given activation degree, using the given implication function
	/// </summary>
	/// <param name="activationDegree">The degree at which to activate the results</param>
	/// <param name="implication">The function to use when calculating the individual term's activation degree</param>
	void Modify(FF_SCALAR activationDegree, const TNorm* implication);

	/// <summary>
	/// Gets whether this consequent value has been properly configured
	/// </summary>
	bool IsValid() const;

	/// <summary>
	/// Parses a consequent value from the given expression, using the engine as context
	/// </summary>
	/// <param name="expression">The expression to parse</param>
	/// <param name="engine">The engine context to work within</param>
	/// <returns>The consequent as parsed from the expression</returns>
	static Consequent* Parse(const std::string& expression, const FuzzyEngine* engine);

private:
	std::vector<Proposition*> myConclusions;
};