/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/

#include "Antecedent .h"

#include <stdexcept>
#include <sstream>
#include <stack>

#include "FuzzyPhi/FuzzyEngine.h"
#include "FuzzyPhi/StringTools.h"
#include "FuzzyPhi/IVariable.h"

Antecedent::Antecedent() :
	myExpression(nullptr) { }

void Antecedent::SetExpression(Expression* value) {
	if (myExpression != nullptr)
		delete myExpression;
	myExpression = value;
}

FF_SCALAR Antecedent::GetActivationDegree(const TNorm * conjuction, const SNorm * disjunction, const Expression* expression) const {
	// Make sure the expression exists before attempting anything
	if (expression == nullptr)
		throw std::runtime_error("Expression is null");

	Expression::Type type = expression->GetType();
	switch (type) {
		// Our type is a proposition (ex: "if 'a is x' then b is y")
		case Expression::Proposition: {
			// Get the expression as a proposition (basically a value or name)
			const Proposition* prop = (const Proposition*)expression;
			// Determine the propositions type (eithier input or output)
			IVariable::Type varType = prop->Variable->GetType();
			FF_SCALAR result = FF_ZERO;

			// If it is an input, we use the term function to evaluate at the input's value
			if (varType == IVariable::Input)
				result = prop->Term->GetMembership(prop->Variable->GetValue());
			// If it is an output, we get the activation degree for the given term from the output's aggregated terms
			else if (varType == IVariable::Output)
				result = ((OutputVariable*)prop->Variable)->GetOutputAggregate()->GetActivationDegree(prop->Term);

			// Return the result
			return result;
		}
		// The expression is an operator (ex: "if 'a is x AND b is y' the c is z")	
		case Expression::Operator: {
			// Cast to an operator
			const Operator* op = (const Operator*)expression;
			Operator::ModeType opType = op->Mode;
			// Switch on the operator mode
			switch (opType) {
				case Operator::AND: {
					// We need both operands
					if (op->Left == nullptr || op->Right == nullptr)
						throw std::invalid_argument("Operator is missing an expression!");
					// Return the conjunction of the two evaluated expressions
					return conjuction->Evaluate(
						GetActivationDegree(conjuction, disjunction, op->Left),
						GetActivationDegree(conjuction, disjunction, op->Right));
					}
				case Operator::OR: {
					// We need both operands
					if (op->Left == nullptr || op->Right == nullptr)
						throw std::invalid_argument("Operator is missing an expression!");
					// Return the disjunction of the two evaluated expressions
					return disjunction->Evaluate(
						GetActivationDegree(conjuction, disjunction, op->Left),
						GetActivationDegree(conjuction, disjunction, op->Right));
				}
				case Operator::NOT: {
					// We only need the left hand operator for nots
					if (op->Left == nullptr)
						throw std::invalid_argument("Operator is missing an expression!");
					// We take the logical not of the value returned from the next expression
					return FF_ONE - GetActivationDegree(conjuction, disjunction, op->Left);
				}
				default:
					break;
			break;
			}
		}
		default:
			throw std::runtime_error("Unknown expression type");
			break;
	}
}

FF_SCALAR Antecedent::GetActivationDegree(const TNorm* conjuction, const SNorm* disjunction) const {
	// Use our internal expression to get the activation degree
	return GetActivationDegree(conjuction, disjunction, myExpression);
}

bool Antecedent::IsValid() const {
	return myExpression != nullptr;
}

Antecedent* Antecedent::Parse(const std::string& expression, const FuzzyEngine* engine) {
	// This is our Working Copy string
	std::string wc = trim_copy(expression);
	if (wc.empty())
		throw std::exception("[Parsing Error] Antecedent string is empty");
	enum State {
		State_Variable = 1,
		State_Is = 2,
		State_Term = 4,
		State_Operator = 8
	};
	// This stores our working proposition
	Proposition* prop = nullptr;
	// Token the string on spaces
	std::istringstream tokenizer(wc);
	std::string token;
	// We use a stack based expression builder, sort of like how some VM's work!
	std::stack<Expression*> expressionBuilder;
	// We start by looking for a variable
	State state = State_Variable;
	// This is our result
	Antecedent* result = new Antecedent();
	try {
		// Get the next word in the expression
		while (tokenizer >> token) {
			// If we are expecting a variable
			if (state & State_Variable) {
				IVariable* var = nullptr;
				// If the engine has in input or output
				if (engine->HasInput(token, &var) || engine->HasOutput(token, &var)) {
					// Make a new proposition with the variable
					prop = new Proposition();
					prop->Variable = var;
					expressionBuilder.push(prop);
					// We now want to look for an is clause
					state = State_Is;
					continue;
				}
			}
			// If we are expecting the is statement
			if (state & State_Is) {
				if (token == "is") state = State_Term;
				continue;
			}
			// If we are expecing a term
			if (state & State_Term) {
				// If our proposition's variable has the term, we grab it, and look for either another variable or an operator
				if (prop->Variable->HasTerm(token, &prop->Term)) {
					state = (State)(State_Variable | State_Operator);
					continue;
				}
			}
			// If we are expecting an operator
			if (state & State_Operator) {
				// These are our 2 operand operators
				if (token == "and" || token == "or") {
					// Make sure we have enough expressions on the stack
					if (expressionBuilder.size() < 2) {
						std::ostringstream ex;
						ex << "[Syntax Error] operator \"" << token << "\" expects two operands, but found " << expressionBuilder.size();
						throw std::exception(ex.str().c_str());
					}
					// Create the new operator
					Operator* op = new Operator();
					// Pop right operator off first
					op->Right = expressionBuilder.top();
					expressionBuilder.pop();
					// Followed by left
					op->Left = expressionBuilder.top();
					expressionBuilder.pop();
					// Determine the mode
					op->Mode = token == "and" ? Operator::AND : Operator::OR;
					// Push op back onto the top of the stack
					expressionBuilder.push(op);
					continue;
				}
				// This is our single operand operator
				else if (token == "not") {
					// Make sure we have a value
					if (expressionBuilder.size() < 1) {
						std::ostringstream ex;
						ex << "[Syntax Error] operator \"" << token << "\" expects one operand, but found none";
						throw std::exception(ex.str().c_str());
					}
					// Create the operator
					Operator* op = new Operator();
					// Grab the top expression
					op->Left = expressionBuilder.top();
					expressionBuilder.pop();
					// Set mode
					op->Mode = Operator::NOT;
					// Push op back onto the top of the stack
					expressionBuilder.push(op);
					continue;
				}
			}
		
			// Should not be possible to get here
			std::ostringstream ex;
			ex << "[Syntax Error] Parse is in unknown state \"" << state << "\" with token \"" << token << "\"";
			throw std::exception(ex.str().c_str());
		}

		// We should end looking for either a variable or an operator
		if (!((state & State_Variable) | (state & State_Operator))) {
			std::ostringstream ex;
			ex << "[Syntax Error] Parse ended in bad state: " << state;
			throw std::exception(ex.str().c_str());
		}

		// We should only have one expression
		if (expressionBuilder.size() != 1) {
			std::ostringstream ex;
			ex << "[Syntax Error] Imbalanced expression tree, ended with " << expressionBuilder.size() << " expressions instead of 1";
			throw std::exception(ex.str().c_str());
		}
	}
	catch (...) {
		delete result;
		throw;
	}
	// Set the expression to the top of the stack (should be the only expression left)
	result->SetExpression(expressionBuilder.top());
	return result;
}
