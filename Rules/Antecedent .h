/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include "FuzzyPhi/Norms.h"
#include "FuzzyPhi/Rules/Expression.h"

class FuzzyEngine;

/// <summary>
/// This is the conditional clause of a rule, for instance, in the expression
/// "if a is x and c is z then b is y"
/// the "a is x and c is z" portion is the antecendent clause
/// </summary>
class Antecedent {
public:
	Antecedent();
	virtual ~Antecedent() { delete myExpression; }

	/// <summary>
	/// Sets the expression to be used by this antecedent
	/// </summary>
	/// <param name="value">The expression to use</param>
	void SetExpression(Expression* value);
	/// <summary>
	/// Gets the expression being used by this antecedent
	/// </summary>
	Expression* GetExpression() const { return myExpression; }

	/// <summary>
	/// Returns how 'truthy' an antecedent clause is. This overload will use the expression set using
	/// the SetExpression method
	/// </summary>
	/// <param name="conjuction">The function to use for conjuctions (fuzzy and)</param>
	/// <param name="disjunction">The function to use for disjunctions (fuzzy or)</param>
	/// <returns>The truthyness for the antecedent's internal function</returns>
	FF_SCALAR GetActivationDegree(const TNorm* conjuction, const SNorm* disjunction) const;
	/// <summary>
	/// Returns how 'truthy' an antecedent clause is
	/// </summary>
	/// <param name="conjuction">The function to use for conjuctions (fuzzy and)</param>
	/// <param name="disjunction">The function to use for disjunctions (fuzzy or)</param>
	/// <param name="expression">The expression to evaluate</param>
	/// <returns>The truthyness for the given expression</returns>
	FF_SCALAR GetActivationDegree(const TNorm* conjuction, const SNorm* disjunction, const Expression* expression) const;

	/// <summary>
	/// Gets whether this antecedent is valid
	/// </summary>
	bool IsValid() const;

	/// <summary>
	/// Parses an antecedent phrase from the given string, using the engine as a context
	/// </summary>
	/// <param name="expression">The expression to parse</param>
	/// <param name="engine">The context to parse in</param>
	/// <returns>The antecedent parsed from the expression</returns>
	static Antecedent* Parse(const std::string& expression, const FuzzyEngine* engine);

private:
	Expression* myExpression;
};