/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/

#include "RuleSet.h"

#include "FuzzyPhi/Rules/Activators/GeneralActivator.h"
#include "FuzzyPhi/FuzzyEngine.h"

RuleSet::RuleSet() { 
	myActivator = nullptr; 
	myConjunction = nullptr;
	myDisjunction = nullptr; 
	myImplication = nullptr;
	myRules = std::vector<Rule*>();
}

RuleSet::~RuleSet() {
	for (int ix = 0; ix < myRules.size(); ix++)
		delete myRules[ix];
	
	delete myActivator;
	delete myConjunction;
	delete myDisjunction;
	delete myImplication;
}

RuleSet* RuleSet::SetImplication(TNorm* value) {
	if (myImplication != nullptr)
		delete myImplication;
	myImplication = value;
	return this;
}

RuleSet* RuleSet::SetConjunction(TNorm* value) {
	if (myConjunction != nullptr)
		delete myConjunction;
	myConjunction = value;
	return this;
}

RuleSet* RuleSet::SetDisjunction(SNorm* value) {
	if (myDisjunction != nullptr)
		delete myDisjunction;
	myDisjunction = value;
	return this;
}

RuleSet* RuleSet::SetActivator(Activator* value) {
	if (myActivator != nullptr)
		delete myActivator;
	myActivator = value;
	return this;
}

Rule* RuleSet::GetRule(int index) const {
		if (index < 0 || index >= myRules.size())
			throw std::out_of_range("Index is outside of the range of rules");

		return myRules[index];
}

Rule* RuleSet::AddRule(Rule* rule) {
	myRules.push_back(rule);
	return rule;
}

Rule* RuleSet::AddRule(const std::string& expression, const FuzzyEngine* engine) {
	Rule* rule = Rule::Parse(expression, engine);
	if (rule != nullptr)
		myRules.push_back(rule);
	return rule;
}

void RuleSet::Activate() {
	// If we have no activator, use a general activator
	if (myActivator == nullptr)
		myActivator = new GeneralActivator();

	// Simply invoke our activator using this as the rule set
	myActivator->Activate(this);
}
