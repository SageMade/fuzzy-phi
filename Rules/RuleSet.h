/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include "FuzzyPhi/Rules/Activator.h"
#include "FuzzyPhi/Norms.h"
#include "FuzzyPhi/Rules/Rule.h"
#include <vector>

class FuzzyEngine;

/// <summary>
/// Represents a collection of rules and operators that work on those rules
/// </summary>
class RuleSet {
public:
	/// <summary>
	/// Creates a new empty rule set
	/// </summary>
	RuleSet();
	~RuleSet();

	/// <summary>
	/// Sets the function that is used when applying activation values to terms in outputs, a good default is new Product
	/// </summary>
	/// <param name="value">The implication function to use</param>
	RuleSet* SetImplication(TNorm* value);
	TNorm* GetImplication() const { return myImplication; }

	/// <summary>
	/// Sets the conjunction function for this rule set, this can be thought of as the AND function
	/// </summary>
	/// <param name="value">The normalized function to use as the conjunction function</param>
	RuleSet* SetConjunction(TNorm* value);
	TNorm* GetConjunction() const { return myConjunction; }

	/// <summary>
	/// Sets the disjunction function for this rule set, this can be thought of as the OR function
	/// </summary>
	/// <param name="value">The normalized function to use as the disjunction function</param>
	RuleSet* SetDisjunction(SNorm* value);
	SNorm* GetDisjunction() const { return myDisjunction; }

	/// <summary>
	/// Sets the activator to be used by this set. A good default value is a new GeneralActivator
	/// </summary>
	/// <param name="value">The activator to use when activating this set</param>
	RuleSet* SetActivator(Activator* value);
	/// <summary>
	/// Gets the activator being used by this set
	/// </summary>
	Activator* GetActivator() const { return myActivator; }

	/// <summary>
	/// Gets the number of rules in this set
	/// </summary>
	size_t NumRules() const { return myRules.size(); }
	/// <summary>
	/// Gets the rule at the given index in this set
	/// </summary>
	/// <param name="index">The index of the rule to get</param>
	/// <returns>The rule at index</returns>
	Rule* GetRule(int index) const;

	/// <summary>
	/// Adds a new rule to this rule set
	/// </summary>
	/// <param name="rule">The rule to be added</param>
	Rule* AddRule(Rule* rule);
	/// <summary>
	/// Parses a string into a rule and adds it to this rule set
	/// </summary>
	/// <param name="expression">The expression to parse into a rule</param>
	/// <param name="engine">The engine context to parse in</param>
	Rule* AddRule(const std::string& expression, const FuzzyEngine* engine);

	void Activate();

protected:
	TNorm* myImplication;
	TNorm* myConjunction;
	SNorm* myDisjunction;

	Activator* myActivator;
	std::vector<Rule*> myRules;
};