/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/

#include "Rule.h"

#include <sstream>
#include <map>
#include <algorithm>
#include "FuzzyPhi/FuzzyEngine.h"
#include "FuzzyPhi/StringTools.h"

Rule::Rule() :
	myAntecedent(nullptr),
	myConsequent(nullptr),
	myWeight(FF_ZERO),
	myActivationDegree(FF_ZERO),
	isTriggered(false) { }

Rule::~Rule() {
	delete myAntecedent;
	delete myConsequent;
}

void Rule::Trigger(const TNorm* implication) {
	// Only trigger if we have been activated
	if (myActivationDegree > 0) {
		// Modify our consequent function by activation degree, using the implication to determine it's strength
		myConsequent->Modify(myActivationDegree, implication);
		isTriggered = true;
	}
}

FF_SCALAR Rule::ActivateWith(const TNorm* conjuction, const SNorm* disjunction) {
	// Update our activation degree by analyzing our condition
	myActivationDegree = myWeight * myAntecedent->GetActivationDegree(conjuction, disjunction);
	return myActivationDegree;
}

void Rule::Reset() {
	myActivationDegree = 0.0f;
	isTriggered = false;
}

bool Rule::IsValid() const {
	return myAntecedent != nullptr && myConsequent != nullptr && myAntecedent->IsValid() && myConsequent->IsValid();
}

void Rule::SetWeight(FF_SCALAR value) {
	if (value <= FF_ZERO)
		throw std::invalid_argument("value must be greater than zero");

	myWeight = value;
}

void Rule::SetAntecedent(Antecedent* value) {
	if (myAntecedent != nullptr)
		delete myAntecedent;
	myAntecedent = value;
}

void Rule::SetConsequent(Consequent* value) {
	if (myConsequent != nullptr)
		delete myConsequent;
	myConsequent = value;
}

Rule* Rule::Parse(const std::string& expression, const FuzzyEngine* engine) {
	// Convert token to lowercase
	std::string wc = lower_copy(expression);
	// Create the tokenizing stream, by default the delimiter is space, so that works for us
	std::istringstream tokenizer(wc);
    
	// This is the default weight if none is specified
	float weight = FF_ONE;

	// These represent the state of our parser
	enum State {
		None,
		If,
		Then,
		With,
		Done
	};
	// Stores the currently parsing token
	std::string token;
	// Stores the current state of the machine
	State state = None;
	// These store the streams for the antecedent and consequent parts of the expression
	std::ostringstream antecedent;
	std::ostringstream consequent;
	// This will be our result
	Rule* result = new Rule();
	try {
		while (tokenizer >> token) {
			switch (state) {
				case None:
					// First token NEEDS to be an if
					if (token == "if") state = If;
					else {
						std::ostringstream ex;
						ex << "[Parsing Error] Expected if keyword, found \"" << token << "\"";
						throw std::exception(ex.str().c_str());
					}
					break;
				case If:
					// Read everything between 'if' and 'then' into the antecedent (condition)
					if (token == "then") state = Then;
					else antecedent << token << " ";
					break;
				case Then:
					// Read everything between 'then' and 'with' into the consequent
					if (token == "with") state = With;
					else consequent << token << " ";
					break;
				case With:
					// Parse the token after 'with' as our scalar type (double or float)
					try {
						weight = FF_PARSE_SCALAR(token); 
						state = Done;
					}
					catch (...) {
						std::ostringstream ex;
						ex << "[Parsing Error] Expected a numerical value for weight, got \"" << token << "\"";
						throw std::exception(ex.str().c_str());
					}
					break;
				case Done: {
					std::ostringstream ex;
					ex << "[Parsing Error] Unexpected token after end of expression: " << token << "";
					throw std::exception(ex.str().c_str());
				}
				default: {
					std::ostringstream ex;
					ex << "[Parsing Error] State machine in invalid state: " << state << "";
					throw std::exception(ex.str().c_str());
				}
			}
		}
		if (state != Done && state != Then) {
			std::ostringstream ex;
			ex << "[Parsing Error] In invalid state after parse: " << state << "";
			throw std::exception(ex.str().c_str());
		}

		// Parse the antecedent and consequent phrases
		result->SetAntecedent(Antecedent::Parse(antecedent.str(), engine));
		result->SetConsequent(Consequent::Parse(consequent.str(), engine));
		// Update the weight
		result->SetWeight(weight);
	}
	catch (...) {
		delete result;
		throw;
	}

	return result;
}
