/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/

#include "OutputVariable.h"

OutputVariable::OutputVariable(const std::string& name /*= ""*/, FF_SCALAR min /*= -FLT_MAX*/, FF_SCALAR max /*= FLT_MAX*/)
	: IVariable(name, min, max),
	myPrevVal(nanf("")),
	myResultRef(nullptr),
	myDefuzzer(nullptr),
	myDefault(0.0f),
	isOutputPinned(true),
	myOutput(nullptr) { 
	SetOutputAggregate(new Aggregate(name, min, max));
}

OutputVariable::~OutputVariable() {
	delete myOutput;
	delete myDefuzzer;
}

OutputVariable* OutputVariable::SetOutputAggregate(Aggregate* value) {
	if (myOutput != nullptr)
		delete myOutput;
	myOutput = value;
	myOutput->SetRange(myMin, myMax);
	return this;
}

IVariable* OutputVariable::SetRange(FF_SCALAR min, FF_SCALAR max) {
	myMin = min < max ? min : max;
	myMin = max > min ? max : min;
	// Make sure our aggregate is also updated
	myOutput->SetRange(min, max);
	return this;
}

OutputVariable* OutputVariable::SetDefuzzer(Defuzzer* value) {
	if (myDefuzzer != nullptr)
		delete myDefuzzer;

	myDefuzzer = value;
	return this;
}

const IFuzzyTerm* OutputVariable::GetHighestTerm() const {
	return myOutput->GetHighestActivated();
}

void OutputVariable::Defuzzify() {
	// If we have a value, store it in prevVal
	if (!FF_ISNAN(myValue))
		myPrevVal = myValue;

	// By default we have no result
	FF_SCALAR result = FF_NAN;
	// Only defuzzify if we have activated outputs
	if (myOutput->NumTerms() > FF_ZERO) {
		// We need a defuzzer!
		if (myDefuzzer != nullptr) {
			result = myDefuzzer->Defuzz(myOutput, myMin, myMax);
		}
		else {
			throw std::runtime_error("The defuzzifier has not been set");
		}
	}
	// We had no activated outputs
	else {
		// If we are pinning our output, use the same value we last calculated that had active outputs
		if (isOutputPinned && !FF_ISNAN(myPrevVal))
			result = myPrevVal;
		// Otherwise use the default value
		else
			result = myDefault;
	}
	myValue = result;

	// Update the reference value if it exists
	if (myResultRef != nullptr)
		*myResultRef = myValue;
}
