/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once

#include <limits>
#include <cmath>
#include <locale>
#include <string>

#define FF_SCALAR double
#define FF_SCALAR_MIN std::numeric_limits<FF_SCALAR>::lowest()
#define FF_SCALAR_MAX std::numeric_limits<FF_SCALAR>::max()

#define FF_ZERO (FF_SCALAR)0.0
#define FF_ONE  (FF_SCALAR)1.0

#define FF_NAN std::numeric_limits<FF_SCALAR>::quiet_NaN()
#define FF_ISNAN(value) std::isnan(value)

#define FF_PARSE_SCALAR (FF_SCALAR)std::stod(token);