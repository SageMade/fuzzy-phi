/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include "IFuzzyTerm.h"

/// <summary>
/// Represents a term that rises between a outer minimum and inner minimum, platues to an inner maximum, then falls between an
/// inner and outer maximum
/// </summary>
class Trapezoid : public IFuzzyTerm {
public:
	Trapezoid(const std::string& name, FF_SCALAR min, FF_SCALAR minInner, FF_SCALAR maxInner, FF_SCALAR max) : IFuzzyTerm(name, min, max) {
		myMinInner = minInner;
		myMaxInner = maxInner;
	}

	inline virtual FF_SCALAR GetMembership(FF_SCALAR x) const override {
		if (x < myMin || x > myMax)
			return FF_ZERO;
		if (x >= myMinInner & x <= myMaxInner)
			return FF_ONE;
		else if (x > myMin & x <= myMinInner) {
			float m = FF_ONE / (myMinInner - myMin);
			return m * x - m * myMin;
		}
		else {
			float m = 1.0f / (myMax - myMaxInner);
			return m * x - m * myMaxInner;
		}
	}

protected:
	FF_SCALAR myMinInner;
	FF_SCALAR myMaxInner;
};