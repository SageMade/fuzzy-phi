/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include "IFuzzyTerm.h"

/// <summary>
/// Represents a simple term function that goes from zero and peaks at 1 at the midpoint between min and max before going back down to zero
///       ^
///      /|\
///     / | \
/// min/  |  \max
/// </summary>
class Triangle : public IFuzzyTerm {
public:
	Triangle(const std::string& name, FF_SCALAR min, FF_SCALAR max) : IFuzzyTerm(name, min, max), myMidpoint((min + max) * 0.5f) { }

	inline virtual FF_SCALAR GetMembership(FF_SCALAR x) const override {
		if (x < this->myMin || x > this->myMax)
			return FF_ZERO;
		else if (x < myMidpoint)
			return (x - this->myMin) / (myMidpoint - this->myMin);
		else if (x >= myMidpoint)
			return (this->myMax - x) / (this->myMax - myMidpoint);
		else
			return FF_ZERO;
	}

	inline static Triangle* New(const std::string& name, FF_SCALAR min, FF_SCALAR max) { return new Triangle(name, min, max); }

protected:
	FF_SCALAR myMidpoint;
};
