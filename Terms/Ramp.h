/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include "IFuzzyTerm.h"

/// <summary>
/// Represents a term that ramps from 0 to 1 over the given range, with 0 at the first value and 1 at the second
/// </summary>
class Ramp : public IFuzzyTerm {
public:
	Ramp(const std::string& name = "", FF_SCALAR min = FF_SCALAR_MIN, FF_SCALAR max = FF_SCALAR_MAX) : IFuzzyTerm(name, min, max) {
		// y = mx + b
		myM = (FF_SCALAR)(1.0 / (max - min));
		myB = -myM * min;
	}
	inline virtual FF_SCALAR GetMembership(FF_SCALAR x) const override {
		if (x < myMin)
			x = myMin;
		else if (x > myMax)
			x = myMax;
		return myM * x + myB;
	}
	
	inline static Ramp* New(const std::string& name = "", FF_SCALAR min = FF_SCALAR_MIN, FF_SCALAR max = FF_SCALAR_MAX) { return new Ramp(name, min, max); }
protected:
	// These are derived from the equation y=mx+b
	FF_SCALAR myM, myB;
};