/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once

#include <string>
#include <typeindex>
#include "FuzzyPhi/StringTools.h"
#include "FuzzyPhi/FuzzyConfig.h"

class SNorm;

/// <summary>
/// The base class for a fuzzy term. This is associated with a name, and can be queried for membership level. Multiple
/// terms make up a fuzzy set (or variable)
/// </summary>
class IFuzzyTerm {
public:
	virtual ~IFuzzyTerm();

	/// <summary>
	/// Gets the minimum extents of this term
	/// </summary>
	FF_SCALAR GetMin() const { return myMin; }
	/// <summary>
	/// Gets the maximum extents of this term
	/// </summary>
	FF_SCALAR GetMax() const { return myMax; }

	/// <summary>
	/// Gets the name of this term. Note that names will always be lower case
	/// </summary>
	const std::string& GetName() const { return myName; }
	/// <summary>
	/// Sets the name of this term. Note that names will always be lower cased
	/// </summary>
	/// <param name="value">The new name for the term</param>
	IFuzzyTerm* SetName(const std::string& value) { myName = lower_copy(value); return this; }
	
	/// <summary>
	/// Gets the raw data of this term's tag
	/// </summary>
	void* GetTagRaw() const { return myTag; }
	template<typename T>
	/// <summary>
	/// Gets the generic tag from this term. If T is not the same as when the tag was set, this will throw an exception
	/// </summary>
	/// <returns>The generic tag of this term cast to T</returns>
	T* GetTag() const {
		if (std::type_index(typeid(T)) != myTagType)
			throw std::runtime_error("Tag type mismatch! Make sure you are retreiving the EXACT same type as you put in");
		return (T*)myTag; 
	}
	template <typename T>
	/// <summary>
	/// Sets a generic tag on this term, can be recalled later using GetTag. Note that owns is true by default, meaning the
	/// term will destroy it's tag upon deletion
	/// </summary>
	/// <param name="value">The value to store in this term's tag</param>
	/// <param name="owns">True if the term owns the value and should delete it upon object death</param>
	/// <returns>The pointer to this term, to allow for chaining</returns>
	IFuzzyTerm* SetTag(T* value, bool owns = true) { 
		myTag = (void*)value; 
		TagDtor = owns ? [](const void* x) { ((T*)x)->~T(); } : nullptr;
		myTagType = std::type_index(typeid(T));
		return this;
	}
	template <typename T>
	/// <summary>
	/// Sets a generic tag on this term, can be recalled later using GetTag. Note that owns is true by default, meaning the
	/// term will destroy it's tag upon deletion
	/// </summary>
	/// <param name="value">The value to store in this term's tag</param>
	/// <param name="owns">True if the term owns the value and should delete it upon object death</param>
	/// <returns>The pointer to this term, to allow for chaining</returns>
	IFuzzyTerm* SetTag(const T& value) {
		myTag = (void*)(new T());
		*((T*)myTag) = value;
		TagDtor = [](const void* x) { ((T*)x)->~T(); };
		myTagType = std::type_index(typeid(T));
		return this;
	}
	template <>
	/// <summary>
	/// Sets a generic tag on this term, can be recalled later using GetTag. Note that because strings are handled with a special case,
	/// the type will actually be an std::string
	/// </summary>
	/// <param name="value">The value to store in this term's tag</param>
	/// <param name="owns">True if the term owns the value and should delete it upon object death</param>
	/// <returns>The pointer to this term, to allow for chaining</returns>
	IFuzzyTerm* SetTag(const char* text, bool owns) {
		// Note
		myTag = new std::string(text);
		TagDtor = [](const void* x) { delete (std::string*)x; };
		myTagType = std::type_index(typeid(std::string));
		return this;
	}

	/// <summary>
	/// Gets the membership value of x within this term, on the range of 0-1
	/// </summary>
	/// <param name="x">The value to resolve</param>
	/// <returns>The level on membership in this term for x</returns>
	virtual FF_SCALAR GetMembership(FF_SCALAR x) const = 0;

	/// <summary>
	/// Outputs a membership graph for this term to the given array, using the specified agregate function
	/// </summary>
	/// <param name="data">The array to write membership data to</param>
	/// <param name="count">The resolution of the array</param>
	/// <param name="aggregateFunc">The function to use for aggregating results, or null for Maximum</param>
	virtual void DumpMembership(float* data, int count, const SNorm* aggregateFunc = nullptr) const;

protected:
	/// <summary>
	/// Internal constructor for fuzzy terms, this takes a name that identifies the term, as well as
	/// a minumum and maximum value for the term's range
	/// </summary>
	/// <param name="name">The name of the term</param>
	/// <param name="min">The minimum value of the term</param>
	/// <param name="max">The maximum value of the term</param>
	IFuzzyTerm(const std::string& name, FF_SCALAR min, FF_SCALAR max) : myTagType(std::type_index(typeid(void))) {
		myName = lower_copy(name);
		myMin = min < max ? min : max;
		myMax = max > min ? max : min;
		myTag = nullptr;
		TagDtor = nullptr;
	}

	FF_SCALAR myMin;
	FF_SCALAR myMax;
	std::string myName;

	// This is the tag stuff, so we can add extra data to terms as we see fit
	void* myTag;
	void(*TagDtor)(const void*);
	std::type_index myTagType;
};

#include "Ramp.h"
#include "Triangle.h"