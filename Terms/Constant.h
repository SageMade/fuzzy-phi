/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#pragma once
#include "IFuzzyTerm.h"
#include "FuzzyPhi/FuzzyConfig.h"

/// <summary>
/// Represents a Term that has a constant value of 1 in it's range, and 0 everywhere else
/// </summary>
class Constant : public IFuzzyTerm {
public:
	Constant(const std::string& name, FF_SCALAR min, FF_SCALAR max) : IFuzzyTerm(name, min, max) { }

	inline virtual FF_SCALAR GetMembership(FF_SCALAR x) const override {
		if (x < myMin || x > myMax)
			return 0.0f;
		else
			return 1.0f;
	}

protected:
	FF_SCALAR myMinInner;
	FF_SCALAR myMaxInner;
};