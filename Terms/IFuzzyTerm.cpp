/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#include "IFuzzyTerm.h"
#include "..\Norms.h"
#include "..\SNorms\Maximum.h"

IFuzzyTerm::~IFuzzyTerm() {
	// If we added a tag
	if (myTag != nullptr) {
		// If we need to destroy the tag, destroy it
		if (TagDtor != nullptr)
			TagDtor(myTag);
		// Nullify the tag
		myTag = nullptr;
	}
}

void IFuzzyTerm::DumpMembership(float* data, int count, const SNorm* aggregateFunc /*= nullptr*/) const {
	// If we do not specify an aggregate function, use Maximum, and delete it once we're done
	bool deleteAggregate = false;
	if (aggregateFunc == nullptr) {
		aggregateFunc = new Maximum;
		deleteAggregate = true;
	}
	// Determine the step size
	FF_SCALAR dx = (myMax - myMin) / count;
	// Iterate over all steps
	for (int ix = 0; ix < count; ix++) {
		// Determine the membership
		FF_SCALAR x = myMin + (ix * dx);
		FF_SCALAR membership = GetMembership(x);
		// Use the aggregate to determine the final membership value
		data[ix] = aggregateFunc->Evaluate(data[ix], membership);
	}

	// Delete temporary aggregate if required
	if (deleteAggregate)
		delete aggregateFunc;
}
