/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include <string>
#include "Aggregate.h"
#include "Defuzzers\Defuzzer.h"

/// <summary>
/// Base class for input and output variables. This is where the crisp values are input and output, and are either
/// fuzzified or defuzzified.
/// </summary>
class IVariable {
public:
	/// <summary>
	/// Represents the type of variable
	/// </summary>
	enum Type {
		Input,
		Output
	};

	virtual ~IVariable() {}

	/// <summary>
	/// Gets the name of this variable. Note that all names are lowecase
	/// </summary>
	virtual std::string GetName() const { return myName; }
	/// <summary>
	/// Sets the name of this variable. Note that all names are automatically lowercased
	/// </summary>
	/// <param name="name">The new name for the variable</param>
	virtual IVariable* SetName(const std::string& name);

	virtual float GetMin() const { return myMin; }
	virtual float GetMax() const { return myMax; }

	/// <summary>
	/// Sets the value of this variable
	/// </summary>
	/// <param name="value">The new value for the variable</param>
	virtual IVariable* SetValue(FF_SCALAR value) { myValue = value; return this; }
	/// <summary>
	/// Gets the crisp value of the variable
	/// </summary>
	virtual FF_SCALAR GetValue() const { return myValue; }

	/// <summary>
	/// Sets the minimum and maximum range of the variable
	/// </summary>
	/// <param name="min">The minimum value of the range</param>
	/// <param name="max">The maximum value of the range</param>
	virtual IVariable* SetRange(FF_SCALAR min, FF_SCALAR max);

	/// <summary>
	/// Adds a new fuzzy term to this variable
	/// </summary>
	/// <param name="term">The term to add</param>
	virtual IFuzzyTerm* AddTerm(IFuzzyTerm* term);
	/// <summary>
	/// Inserts a new fuzzy term into this variable at the given index
	/// </summary>
	/// <param name="term">The term to add</param>
	/// <param name="index">The index to insert at, this will move all elements at and after index 1 index up</param>
	virtual IVariable* InsertTerm(IFuzzyTerm* term, size_t index);
	/// <summary>
	/// Gets the fuzzy term at the given index
	/// </summary>
	/// <param name="index">The index of the term to get</param>
	/// <returns>The term at index</returns>
	virtual IFuzzyTerm* GetTerm(size_t index) const;
	/// <summary>
	/// Removes the term at the given index from this variable
	/// </summary>
	/// <param name="index">The index of the term to remove</param>
	/// <returns>The term that was removed (will not be deleted)</returns>
	virtual IFuzzyTerm* RemoveTerm(size_t index);
	/// <summary>
	/// Gets the number of terms in this variable
	/// </summary>
	virtual size_t NumTerms() const { return myTerms.size(); }

	/// <summary>
	/// Checks whether this variable has a term by the given name, and returns it if applicable
	/// </summary>
	/// <param name="name">The name of the term to search for</param>
	/// <param name="out">A pointer to a fuzzy term pointer that will store the result. Will not be modified if returns false</param>
	/// <returns>True if this variable has a term who's name matches name, false if otherwise</returns>
	virtual bool HasTerm(const std::string& name, IFuzzyTerm** out = nullptr) const;
		
	/// <summary>
	/// Gets the type of this variable (either input or output)
	/// </summary>
	virtual Type GetType() const = 0;
	
protected:
	IVariable(const std::string& name, FF_SCALAR min, FF_SCALAR max);

	std::string myName;
	std::vector<IFuzzyTerm*> myTerms;
	FF_SCALAR myValue;
	FF_SCALAR myMin, myMax;
};

#include "InputVariable.h"
#include "OutputVariable.h"