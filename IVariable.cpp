/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/

#include "IVariable.h"
#include <algorithm>
#include "StringTools.h"

IVariable* IVariable::SetName(const std::string& name) {
	// Names are always lower case
	myName = lower_copy(name);
	return this;
}

IVariable* IVariable::SetRange(FF_SCALAR min, FF_SCALAR max) {
	myMin = min < max ? min : max;
	myMax = max > min ? max : min;
	return this;
}

IFuzzyTerm* IVariable::AddTerm(IFuzzyTerm* term) {
	myTerms.push_back(term);
	return term;
}

IVariable* IVariable::InsertTerm(IFuzzyTerm* term, size_t index) {
	if (index > myTerms.size())
		throw std::out_of_range("Index is out of range of the list");
	myTerms.insert(myTerms.begin() + index, term);
	this;
}

IFuzzyTerm* IVariable::GetTerm(size_t index) const {
	if (index >= myTerms.size())
		throw std::out_of_range("Index is out of range of the list");
	return myTerms[index];
}

IFuzzyTerm* IVariable::RemoveTerm(size_t index) {
	if (index >= myTerms.size())
		throw std::out_of_range("Index is out of range of the list");
	IFuzzyTerm* result = myTerms[index];
	myTerms.erase(myTerms.begin() + index);
	return result;
}

bool IVariable::HasTerm(const std::string& name, IFuzzyTerm** out /*= nullptr*/) const {
	// Iterate over allterms, match on name
	for (int ix = 0; ix < myTerms.size(); ix++) {
		if (myTerms[ix]->GetName() == name) {
			if (out != nullptr) *out = myTerms[ix];
			return true;
		}
	}
	return false;
}

IVariable::IVariable(const std::string& name, FF_SCALAR min, FF_SCALAR max) :
	myValue(FF_NAN)
{
	// Names are always lowercase
	myName = lower_copy(name);
	myMin = min < max ? min : max;
	myMax = max > min ? max : min;
}

