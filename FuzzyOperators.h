/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once

// No longer used, for future reference only

#ifdef FUZZY_MULTIPLY
#define FUZZY_AND(x, y) (x * y)
#define FUZZY_OR(x, y) (x + y - x * y)
#else
#define FUZZY_AND(x, y) (x < y ? x : y)
#define FUZZY_OR(x, y) (x > y ? x : y)
#endif

#define FUZZY_NOT(x) (1.0f - x)
#define FUZZY_NAND(x,y) (FUZZY_NOT(FUZZY_AND(x, y)))
#define FUZZY_XOR(x, y) (FUZZY_AND(FUZZY_OR(x, y), FUZZY_NAND(x, y)))