/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once

#include <algorithm>
#include <string>
#include <cctype>
#include <locale>

// Many of these string operations are borrowed from
// https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring

inline void ltrim(std::string& value) {
	value.erase(value.begin(), std::find_if(value.begin(), value.end(), [](int ch) {
		return !std::isspace(ch);
	}));
}

inline void rtrim(std::string& value) {
	value.erase(std::find_if(value.rbegin(), value.rend(), [](int ch) {
		return !std::isspace(ch);
	}).base(), value.end());
}

inline void trim(std::string& value) {
	ltrim(value);
	rtrim(value);
}

inline std::string ltrim_copy(std::string s) {
	ltrim(s);
	return s;
}

inline std::string rtrim_copy(std::string s) {
	rtrim(s);
	return s;
}

inline std::string trim_copy(std::string s) {
	trim(s);
	return s;
}

inline void lower(std::string& string) {
	std::transform(string.begin(), string.end(), string.begin(), tolower);
}

inline std::string lower_copy(std::string string) {
	std::transform(string.begin(), string.end(), string.begin(), tolower);
	return string;
}

inline std::string upper(std::string& string) {
	std::transform(string.begin(), string.end(), string.begin(), toupper);
	return string;
}

inline std::string upper_copy(std::string string) {
	std::transform(string.begin(), string.end(), string.begin(), toupper);
	return string;
}