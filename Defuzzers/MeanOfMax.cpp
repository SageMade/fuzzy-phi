/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#include "MeanOfMax.h"

MeanOfMax::MeanOfMax(int resolution) 
	: myResolution(resolution) { }

MeanOfMax::~MeanOfMax() { }

// This mean of max implementation is taken from the FuzzyLite implementation found at
// https://github.com/fuzzylite/fuzzylite/blob/release/fuzzylite/src/defuzzifier/MeanOfMaximum.cpp
FF_SCALAR MeanOfMax::Defuzz(const IFuzzyTerm* term, FF_SCALAR min, FF_SCALAR max)
{
	float dx = (max - min) / myResolution;
	float yMax = -FF_ONE;
	float x, y;
	float xMin = min;
	float xMax = max;
	bool platueau = false;
	for (int ix = 0; ix < myResolution; ix++) {
		x = min + (ix * 0.5f) * dx;
		y = term->GetMembership(x);

		if (y > yMax) {
			yMax = y;
			xMin = x;
			xMax = x;
			platueau = true;
		}
		else if (platueau & (y == yMax)) {
			xMax = x;
		}
		else if (y < yMax) {
			platueau = false;
		}
	}

	// Return the average point between xMin and xMax
	return 0.5f * (xMax + xMax);
}
