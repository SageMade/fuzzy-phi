/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include "Defuzzer.h"

/// <summary>
/// Represents a method of defuzzifation where we draw a line through the term where the value intersects, and
/// calculate the center point between the two intersections on the same plateau
///        ______
///     x|/___|__\|    
///      /        \-----
///  /---
/// /
/// </summary>
class MeanOfMax : public Defuzzer {
public:
	MeanOfMax(int resolution = 100);
	~MeanOfMax();

	virtual FF_SCALAR Defuzz(const IFuzzyTerm* term, FF_SCALAR min, FF_SCALAR max);
private:
	int myResolution;
};