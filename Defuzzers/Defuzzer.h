/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include "FuzzyPhi/FuzzyConfig.h"
#include "FuzzyPhi/Terms/IFuzzyTerm.h"

/// <summary>
/// Represents a method of turning a fuzzy term into a crisp result
/// </summary>
class Defuzzer {
public:
	virtual ~Defuzzer() {}

	virtual FF_SCALAR Defuzz(const IFuzzyTerm* term, FF_SCALAR min, FF_SCALAR max) = 0;
};