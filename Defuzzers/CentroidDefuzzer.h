/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include "Defuzzer.h"
#include "FuzzyPhi/FuzzyConfig.h"

/// <summary>
/// Represents a defuzzing technique that attempts to get the average of the
/// term based on the area under the term
/// </summary>
class CentroidDefuzzer : public Defuzzer {
public:
	CentroidDefuzzer(int resolution = 100);
	~CentroidDefuzzer() {}

	virtual FF_SCALAR Defuzz(const IFuzzyTerm* term, FF_SCALAR min, FF_SCALAR max);
private:
	int myResolution;
};