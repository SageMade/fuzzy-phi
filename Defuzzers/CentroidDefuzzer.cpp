/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#include "CentroidDefuzzer.h"

CentroidDefuzzer::CentroidDefuzzer(int resolution /*= 100*/) : myResolution(resolution < 1 ? 1 : resolution) { }

// This centroid implementation is taken from the FuzzyLite implementation found at
// https://github.com/fuzzylite/fuzzylite/blob/release/fuzzylite/src/defuzzifier/Centroid.cpp
FF_SCALAR CentroidDefuzzer::Defuzz(const IFuzzyTerm * term, FF_SCALAR min, FF_SCALAR max) {
	FF_SCALAR dx = (max - min) / myResolution;
	FF_SCALAR x, y;
	FF_SCALAR area{ 0 }, xCentroid{ 0 };
	for (int ix = 0; ix < myResolution; ix++) {
		x = min + (ix + 0.5f) * dx;
		y = term->GetMembership(x);

		xCentroid += y * x;
		area += y;
	}
	return xCentroid / area;
}
