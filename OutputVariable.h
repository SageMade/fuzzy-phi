/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include "IVariable.h"

/// <summary>
/// Represents a variable that will have a crisp value calculated depending on the inputs and the rules that have been defined
/// </summary>
class OutputVariable : public IVariable {
public:
	OutputVariable(const std::string& name = "", FF_SCALAR min = FF_SCALAR_MIN, FF_SCALAR max = FF_SCALAR_MAX);
	virtual ~OutputVariable();

	/// <summary>
	/// Gets the aggregate for this output
	/// </summary>
	Aggregate* GetOutputAggregate() const { return myOutput; }
	/// <summary>
	/// Sets the aggregate to use for this output. Note that this will override the aggregate's range
	/// </summary>
	/// <param name="value">The new aggregate for this output</param>
	OutputVariable* SetOutputAggregate(Aggregate* value);

	/// <summary>
	/// Sets the range for this output variable and it's underlying aggregate
	/// </summary>
	/// <param name="min">The minimum extents of the range</param>
	/// <param name="max">The maximum extents of the range</param>
	virtual IVariable* SetRange(FF_SCALAR min, FF_SCALAR max) override;

	/// <summary>
	/// Gets the defuzzer being used by this output variable
	/// </summary>
	Defuzzer* GetDefuzzer() const { return myDefuzzer; }
	/// <summary>
	/// Sets the defuzzer to use for this output variable, note that the output variable will take ownership of the defuzzer
	/// defuzzer
	/// </summary>
	/// <param name="value">The new defuzzer to use</param>
	OutputVariable* SetDefuzzer(Defuzzer* value);

	/// <summary>
	/// Gets the term that had the highest activation level
	/// </summary>
	/// <returns>The term in the aggregated set with the highest activation level</returns>
	const IFuzzyTerm* GetHighestTerm() const;

	/// <summary>
	/// Defuzzifies this output, and updates it's value
	/// </summary>
	void Defuzzify();

	/// <summary>
	/// Gets the pointer that will be updated with the result of this output's fuzzy variable
	/// </summary>
	/// <param name="value">The pointer to the floating point value that will accept the result, or null to ignore</param>
	OutputVariable* SetResultRef(FF_SCALAR* value) { myResultRef = value; return this; }
	/// <summary>
	/// Gets the pointer to the output value that will be updated when this output changes
	/// </summary>
	FF_SCALAR* GetResultRef() const { return myResultRef; }

	/// <summary>
	/// Sets the default value to use if output pinning is disabled and there are no terms activated
	/// </summary>
	/// <param name="value">The default value if no terms are activated</param>
	/// <returns>This pointer, to allow for chaining</returns>
	OutputVariable* SetDefault(FF_SCALAR value) { myDefault = value; return this; }
	/// <summary>
	/// Gets the default value used when output pinning is disabled and no terms are activated
	/// </summary>
	FF_SCALAR GetDefault() const { return myDefault; }

	/// <summary>
	/// Sets whether or not output pinning is enabled. If set to true and no terms are activated, then it will use
	/// the previous value that has been calculated
	/// </summary>
	/// <param name="enabled">True if output pinning is enabled</param>
	/// <returns>This pointer, to allow for chaining</returns>
	OutputVariable* SetOutputPinning(bool enabled) { isOutputPinned = enabled; return this; }
	/// <summary>
	/// Gets whether or not output pinning is enabled or not
	/// </summary>
	bool IsOutputPinningEnabled() const { return isOutputPinned; }

	virtual Type GetType() const { return Output; };


private:
	Aggregate* myOutput;
	Defuzzer*  myDefuzzer;

	bool isOutputPinned;
	FF_SCALAR myPrevVal;
	FF_SCALAR myDefault;
	FF_SCALAR* myResultRef;
};

