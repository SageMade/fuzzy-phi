/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once
#include <string>
#include "FuzzyPhi/FuzzyConfig.h"

/// <summary>
/// http://www.nicodubois.com/bois5.2.htm
/// </summary>
class TNorm {
public:
	virtual ~TNorm() { }
	virtual FF_SCALAR Evaluate(FF_SCALAR a, FF_SCALAR b) const = 0;
	virtual const std::string& GetName() const = 0;
};

class SNorm {
public:
	virtual ~SNorm() { }
	virtual FF_SCALAR Evaluate(FF_SCALAR a, FF_SCALAR b) const = 0;
	virtual const std::string& GetName() const = 0;
};