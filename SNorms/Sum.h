/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/

#pragma once
#include "FuzzyPhi/Norms.h"

/// <summary>
/// A function that represents the S-Norm of a sum of the 2 arguments, this expands to
/// a + b - (a * b)
/// </summary>
class Sum : public SNorm {
public:
	inline virtual FF_SCALAR Evaluate(FF_SCALAR a, FF_SCALAR b) const { return a + b - (a * b); };
	inline virtual const std::string& GetName() const { return "Sum"; };
};