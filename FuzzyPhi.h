/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/
#pragma once

#include "FuzzyPhi/FuzzyEngine.h"
#include "FuzzyPhi/IVariable.h"
#include "FuzzyPhi/Rules/Activators/GeneralActivator.h"
#include "FuzzyPhi/Rules/RuleSet.h"
#include "FuzzyPhi/Defuzzers/CentroidDefuzzer.h"
#include "FuzzyPhi/Defuzzers/MeanOfMax.h"
#include "FuzzyPhi/TNorms/Minimum.h"
#include "FuzzyPhi/TNorms/Product.h"
#include "FuzzyPhi/SNorms/Maximum.h"
#include "FuzzyPhi/SNorms/Sum.h"