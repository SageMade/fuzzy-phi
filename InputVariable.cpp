/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/

#include "InputVariable.h"

void InputVariable::Update() {
	if (myValueRef != nullptr)
		myValue = *myValueRef;
}
