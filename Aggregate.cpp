/*
	Date: March 18, 2019
	Authors:
	   Shawn Matthews  - 100412327
	   Daniel Munusami - 100552012
*/

#include "Aggregate.h"
#include "SNorms\Maximum.h"

Aggregate::Aggregate(const std::string& name, FF_SCALAR min, FF_SCALAR max) : IFuzzyTerm(name, min, max) {
	myAggregation = new Maximum();
}

Aggregate::~Aggregate() {
	delete myAggregation;
	#ifdef _DEBUG
	memset(this, 0xCD, sizeof(Aggregate));
	#endif
}

FF_SCALAR Aggregate::GetActivationDegree(const IFuzzyTerm* term) const {
	// Iterate over all terms in this aggregate
	for (int ix = 0; ix < myTerms.size(); ix++) {
		// If the terms match, return the term's degree
		if (myTerms[ix].Term == term)
			return myTerms[ix].Degree;
	}
	// Default result is 0
	return FF_ZERO;
}

const IFuzzyTerm* Aggregate::GetHighestActivated() const {
	// Return a nullptr if empty
	const IFuzzyTerm* result = nullptr;
	// Start from the lowest possible value
	FF_SCALAR max = FF_SCALAR_MIN;
	// Iterate over all terms
	for (int ix = 0; ix < myTerms.size(); ix++) {
		// If the degree is larger, pick it from the set
		if (myTerms[ix].Degree > max) {
			max = myTerms[ix].Degree;
			result = myTerms[ix].Term;
		}
	}
	return result;
}

void Aggregate::AddTerm(const IFuzzyTerm* term, FF_SCALAR activationDegree, const TNorm* implication) {
	// Iterate over all our existing terms
	for (int ix = 0; ix < myTerms.size(); ix++) {
		// If we already have a entry for this term
		if (myTerms[ix].Term == term) {
			// If we have an aggregation function defined, use it to aggregate our results
			if (myAggregation != nullptr)
				myTerms[ix].Degree = myAggregation->Evaluate(activationDegree, myTerms[ix].Degree);
			// Otherwise sum
			else
				myTerms[ix].Degree += activationDegree;
			// Early bail
			return;
		}
	}
	// No entries were found with that term, add one
	myTerms.push_back(ActivatedTerm(term, activationDegree, implication));
}

const Aggregate::ActivatedTerm& Aggregate::RemoveTerm(int index) {
	if (index < 0 || index >= myTerms.size())
		throw std::out_of_range("Index is out of range");

	// Get the term, remove it, and return
	const ActivatedTerm& term = myTerms[index];
	myTerms.erase(myTerms.begin() + index);
	return term;
}

const Aggregate::ActivatedTerm& Aggregate::GetTerm(int index) const {
	if (index < 0 || index >= myTerms.size())
		throw std::out_of_range("Index is out of range");
	return myTerms[index];
}

void Aggregate::Clear() {
	myTerms.clear();
}

FF_SCALAR Aggregate::GetMembership(FF_SCALAR x) const {
	if (myAggregation == nullptr)
		throw new std::runtime_error("Aggregation function has not been defined!");
	// If we have no terms, our membership does not exist (is not 0, because 0 is still a valid membership)
	if (myTerms.empty())
		return FF_NAN;

	FF_SCALAR result = FF_ZERO;
	// Iterate over all terms in this set
	for (int ix = 0; ix < myTerms.size(); ix++) {
		// Use the aggregation function to combine membership values
		result = myAggregation->Evaluate(result, myTerms[ix].CalcMembership(x));
	}
	return result;
}

void Aggregate::SetRange(FF_SCALAR min, FF_SCALAR max) {
	myMin = min < max ? min : max;
	myMax = max > min ? max : min;
}

FF_SCALAR Aggregate::ActivatedTerm::CalcMembership(FF_SCALAR x) const {
	if (Term == nullptr)
		throw std::runtime_error("Term cannot be null");
	if (Implication == nullptr)
		throw std::runtime_error("Implication cannot be null");

	// Use our implication function to evaluate our degree in respect to our term's membership at X.
	// essentially this is saying, we have been activated by this much, and our term here has this value,
	// so how much should our output be
	return Implication->Evaluate(Term->GetMembership(x), Degree);
}
